require('app-module-path').addPath(__dirname);
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');

//mongoose configuration
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var config = require('../mongoose.config');
var winston = require('winston');
winston.add(winston.transports.File, { filename: 'log/app.log' });

var port = config.port || 8080;
mongoose.connect(config.database);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(require('app/controllers'));


app.listen(port, function () {
    console.log('Magic happens on port ' + port)
});