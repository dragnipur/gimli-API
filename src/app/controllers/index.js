let express = require('express');
let router = express.Router();
let auth = require('app/middlewares/authInterceptor');
let userController = require('app/controllers/userController');
let teamController = require('app/controllers/teamController');

router.post('/auth/register', (req, res) => userController.register(req, res));
router.post('/auth/login', (req, res) => userController.login(req, res));

router.post('/team', auth, (req, res) => teamController.createTeam(req, res));
router.get('/team', auth, (req, res) => teamController.fetchTeams(req, res));

router.post('/team/members', auth, (req, res) => teamController.addTeamMember(req, res));
router.get('/team/:teamId/members', auth, (req, res) => teamController.fetchTeamMembers(req, res));

module.exports = router;
