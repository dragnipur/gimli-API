var jwt = require('jsonwebtoken');
var User = require('app/models/user');
var config = require('../../../mongoose.config');
var hasher = require('password-hasher');
var winston = require('winston');

const emailAddressRegex = /(.+)@(.+){2,}\.(.+){2,}/;
const passwordRegex = /^[a-zA-Z0-9-!@#$%&*?.,]*$/;
const hashAlgorithm = 'ssha256';
const encoding = 'hex';
const oneDay = '1d';

module.exports = {
    register: register,
    login: login
};

function register(req, res) {
    if (registrationRequestValid(req)) {
        User.findOne({
            emailAddress: req.body.emailAddress
        }).then(
            (user) => {
                if (user) {
                    res.status(409).send();
                } else {
                    createUser(req, res);
                }
            },
            (err) => {
                handleError(err, res)
            });
    } else {
        res.status(400).send();
    }
}

function login(req, res) {
    User.findOne({
        emailAddress: req.body.emailAddress
    }).then(
        (user) => {
            if (!user) {
                res.status(400).send();
            } else {
                var buffer = new Buffer(user.salt, encoding);
                var hashedPassword = hasher.createHash(hashAlgorithm, req.body.password, buffer);
                if (user.password === hashedPassword.hash.toString(encoding)) {
                    var token = jwt.sign({userId: user._id}, config.secret, {
                        expiresIn: oneDay
                    });
                    res.status(200).json({token: token});
                } else {
                    res.status(400).send();
                }
            }
        },
        (err) => {
            handleError(err, res)
        });
}

function registrationRequestValid(req) {
    if (!req.body.emailAddress || !req.body.password || !req.body.password2) return false;
    return (req.body.password === req.body.password2 &&
    req.body.emailAddress.length < 64 &&
    req.body.password.length < 32 && req.body.password.length >= 8 &&
    emailAddressRegex.test(req.body.emailAddress) &&
    passwordRegex.test(req.body.password));
}

function createUser(req, res) {
    var hashedPassword = hasher.createHashAndSalt(hashAlgorithm, req.body.password, 10);

    var newUser = new User({
        emailAddress: req.body.emailAddress,
        registrationDate: new Date(),
        password: hashedPassword.hash.toString(encoding),
        salt: hashedPassword.salt.toString(encoding)
    });

    newUser.save().then(
        () => {
            res.status(200).send();
        },
        (err) => {
            handleError(err, res);
        });
}

function handleError(err, res) {
    winston.log('error', err);
    res.status(500).send();
}
