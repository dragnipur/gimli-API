var Team = require('app/models/team');
var TeamMember = require('app/models/teamMember');
var winston = require('winston');

module.exports = {
    createTeam: createTeam,
    fetchTeams: fetchTeams,
    addTeamMember: addTeamMember,
    fetchTeamMembers: fetchTeamMembers
};

function fetchTeams(req, res) {
    Team.find({
        userId: req.token.userId
    }).then(
        (teams) => {
            res.status(200).json({teams: teams});
        },
        (err) => {
            handleError(err, res);
        })
}

function createTeam(req, res) {
    if (teamValid(req)) {
        var team = new Team({
            userId: req.token.userId,
            name: req.body.name,
            description: req.body.description || ''
        });

        team.save().then(
            () => {
                res.status(200).send();
            },
            (err) => {
                handleError(err, res);
            });
    } else {
        res.status(400).send();
    }
}

function teamValid(req) {
    return !!req.body.name;
}

function fetchTeamMembers(req, res) {
    Team.findOne({
        _id: req.params.teamId
    }).then(
        (team) => {
            if (!team || req.token.userId !== team.userId) res.status(400).send();
            returnTeamMembers(req, res);
        },
        (err) => handleError(err, res)
    )
}

function returnTeamMembers(req, res) {
    TeamMember.find({
        teamId: req.body.teamId
    }).then(
        (teamMembers) => {
            res.status(200).json({teamMembers: teamMembers});
        },
        (err) => handleError
    )
}

function addTeamMember(req, res) {
    if (teamMemberRequestValid(req)) {
        Team.findOne({
            _id: req.body.teamId
        }).then(
            (team) => {
                if (!team || req.token.userId !== team.userId) res.status(400).send();
                createTeamMember(req, res);
            },
            (err) => handleError(err, res)
        )
    } else {
        res.status(400).send();
    }
}

function teamMemberRequestValid(req) {
    return !!(req.body.teamId && req.body.name && req.body.name.length < 32);
}

function createTeamMember(req, res) {
    var teamMember = new TeamMember({
        teamId: req.body.teamId,
        title: req.body.title
    });

    teamMember.save().then(
        () => {
            res.status(200).send();
        },
        (err) => handleError(err, res)
    )
}

function handleError(err, res) {
    winston.log('error', err);
    res.status(500).send();
}