var jwt = require('jsonwebtoken');
var config = require('../../../mongoose.config');

module.exports = function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) {
                res = res.status(401).send();
            } else {
                req.token = decoded;
                next();
            }
            return res;
        });
    } else {
         res.status(401).send();
    }
};
