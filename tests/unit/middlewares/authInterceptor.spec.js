require('mocha');
require('should');
var mock = require('mock-require');
var chai = require('chai');
var sinon = require('sinon');
var mongoose = require('mongoose');
require('sinon-mongoose');
var helper = require('../../require_helper');
var sinonChai = require("sinon-chai");
var expect = chai.expect;
chai.use(sinonChai);

describe('authInterceptor', function () {
    var authInterceptor;
    var callback, res, next;
    var error, value;

    beforeEach(() => {
        setupMocks();
        authInterceptor = helper('middlewares/authInterceptor');
        value = error = undefined;
    });

    it('should set the decoded userId on the request and perform the next method when a valid token is supplied in the request body.', () => {
        var req = {body: {token: 'abc'}};
        value = '12345';

        authInterceptor = new authInterceptor(req, res, next);

        expect(req.token).to.equal('12345');
        expect(next).to.have.been.called;
    });

    it('should set the decoded userId on the request and perform the next method when a valid token is supplied as query param.', () => {
        var req = {body: {}, query: {token: 'abc'}};
        value = '12345';

        authInterceptor = new authInterceptor(req, res, next);

        expect(req.token).to.equal('12345');
        expect(next).to.have.been.called;
    });

    it('should set the decoded userId on the request and perform the next method when a valid token is supplied as a header.', () => {
        var req = {body: {}, query: {}, headers: {'x-access-token': '12345'}};
        value = '12345';

        authInterceptor = new authInterceptor(req, res, next);

        expect(req.token).to.equal('12345');
        expect(next).to.have.been.called;
    });

    it('should send back statusCode 401 when no token is supplied to the authInterceptor', () => {
        var req = {body: {}, query: {}, headers: {}};

        authInterceptor = new authInterceptor(req, res, next);

        expect(res.status).to.have.been.calledWith(401);
        expect(res.send).to.have.been.called;
    });

    it('should send back statusCode 401 when a token is supplied but an error is thrown while decrypting it.', () => {
        var req = {body: { token: 'abc'}, query: {}, headers: {}};
        error =  'abc';
        
        authInterceptor = new authInterceptor(req, res, next);

        expect(res.status).to.have.been.calledWith(401);
        expect(res.send).to.have.been.called;
    });

    function setupMocks() {
        mock('jsonwebtoken', {
            verify: function (token, secret, callback) {
                callback(error, value);
            }
        });

        mock('config', {secret: '123'});

        res = {};
        res.status = sinon.stub().returns(res);
        res.send = sinon.spy();
        next = sinon.spy();
    }

});