require('mocha');
require('should');
var mock = require('mock-require');
var chai = require('chai');
var sinon = require('sinon');
var mongoose = require('mongoose');
require('sinon-mongoose');
var helper = require('../../require_helper');
var sinonChai = require("sinon-chai");
var expect = chai.expect;
chai.use(sinonChai);

describe('TeamController', function () {
    var teamController;
    var saveSuccess, saveError;
    var findSuccess, findError;

    beforeEach(() => {
        setupMocks();
        teamController = helper('controllers/teamController');
    });

    it('should create a new team and return status 200 when a valid team creation request is received with a team description.', () => {
        var req = teamReq();
        var res = teamRes();

        teamController.createTeam(req, res);
        saveSuccess();

        expect(res.status).to.have.been.calledWith(200);
        expect(res.send).to.have.been.called;
    });

    it('should create a new team and return status 200 when a valid team creation request is received without a team description.', () => {
        var req = teamReq();
        req.body.description = undefined;
        var res = teamRes();

        teamController.createTeam(req, res);
        saveSuccess();

        expect(res.status).to.have.been.calledWith(200);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 if a user attempts to create a team without a name.', () => {
        var req = teamReq();
        req.body.name = undefined;
        var res = teamRes();

        teamController.createTeam(req, res);
        saveSuccess();

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 500 if an internal server error is encountered while saving a new team.', () => {
        var req = teamReq();
        var res = teamRes();

        teamController.createTeam(req, res);
        saveError('team - saving new team');

        expect(res.status).to.have.been.calledWith(500);
        expect(res.send).to.have.been.called;
    });

    it('should return the user\'s teams and status 200 when they are requested and successfully retrieved.', () => {
        var req = teamReq();
        var res = teamRes();

        teamController.fetchTeams(req, res);
        findSuccess('teams');

        expect(res.status).to.have.been.calledWith(200);
        expect(res.json).to.have.been.calledWith({teams: 'teams'});
    });

    it('should return status 500 when an internal server error is encountered while retrieving a user\'s teams.', () => {
        var req = teamReq();
        var res = teamRes();

        teamController.fetchTeams(req, res);
        findError('team - fetching a user\'s teams');

        expect(res.status).to.have.been.calledWith(500);
        expect(res.send).to.have.been.called;
    });

    it('should return status 200 when a valid teamMember is added to an existing team.', () => {
        var req = {token: '123', body: {teamId: '12345', name: 'Pieter'}};
        var res = teamRes();

        teamController.addTeamMember(req, res);
        findSuccess({userId: '123'});
        saveSuccess();

        expect(res.status).to.have.been.calledWith(200);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when an invalid teamMember is added to an existing team.', () => {
        var req = {body: {}};
        var res = teamRes();

        teamController.addTeamMember(req, res);

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when a valid teamMember is added to a team that isn\'t owned by the user.', () => {
        var req = {token: '123', body: {teamId: '12345', name: 'Pieter'}};
        var res = teamRes();

        teamController.addTeamMember(req, res);
        findSuccess({userId: '321'});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when a valid teamMember is added to a non-existing team.', () => {
        var req = {token: '123', body: {teamId: '12345', name: 'Pieter'}};
        var res = teamRes();

        teamController.addTeamMember(req, res);
        findSuccess();

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 500 when a valid teamMember is added but an internal error is thrown while finding the matching team.', () => {
        var req = {token: '123', body: {teamId: '12345', name: 'Pieter'}};
        var res = teamRes();

        teamController.addTeamMember(req, res);
        findError('teamMember - find team');

        expect(res.status).to.have.been.calledWith(500);
        expect(res.send).to.have.been.called;
    });

    it('should return status 500 when a valid teamMember is added but an internal error is thrown while saving the teamMember.', () => {
        var req = {token: '123', body: {teamId: '12345', name: 'Pieter'}};
        var res = teamRes();

        teamController.addTeamMember(req, res);
        findSuccess({userId: '123'});
        saveError('teamMember - save teamMember');

        expect(res.status).to.have.been.calledWith(500);
        expect(res.send).to.have.been.called;
    });

    function setupMocks() {
        mock('app/models/team', Team);
        function Team() {
            return {
                save: function () {
                    return {
                        then: function (success, error) {
                            saveSuccess = success;
                            saveError = error;
                        }
                    }
                }
            }
        }

        Team.find = function () {
            return {
                then: function (success, error) {
                    findSuccess = success;
                    findError = error;
                }
            };
        };

        mock('app/models/teamMember', TeamMember);
        function TeamMember() {
            return {
                save: function () {
                    return {
                        then: function (success, error) {
                            saveSuccess = success;
                            saveError = error;
                        }
                    }
                }
            }
        }
    }

    function teamReq() {
        return {
            token: {userId: '12345'},
            body: {
                name: 'team',
                description: 'description'
            }
        };
    }

    function teamRes() {
        var res = {};
        res.status = sinon.stub().returns(res);
        res.send = sinon.spy();
        res.json = sinon.spy();
        return res;
    }
});