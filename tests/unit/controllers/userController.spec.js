require('mocha');
require('should');
var mock = require('mock-require');
var chai = require('chai');
var sinon = require('sinon');
var mongoose = require('mongoose');
require('sinon-mongoose');
var helper = require('../../require_helper');
var sinonChai = require("sinon-chai");
var expect = chai.expect;
chai.use(sinonChai);

describe('UserController', function () {
    var userController;
    var saveSuccess, saveError;
    var findSuccess, findError;

    beforeEach(() => {
        setupMocks();
        userController = helper('controllers/userController');
    });

    it('should successfully register a user when a valid email address and password are supplied and the email address isn\'t taken', () => {
        var req = registrationReq();
        var res = authRes();

        userController.register(req, res);
        findSuccess();
        saveSuccess();

        expect(res.status).to.have.been.calledWith(200);
        expect(res.send).to.have.been.called;
    });

    it('should return status 409 when the email address a user wants to register with already exists.', () => {
        var req = registrationReq();
        var res = authRes();

        userController.register(req, res);
        findSuccess({});

        expect(res.status).to.have.been.calledWith(409);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when the registration email address is not in the format XX@XX.XX', () => {
        var req = registrationReq();
        req.body.emailAddress = 'test.com';
        var res = authRes();

        userController.register(req, res);
        findSuccess({});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when the registration email address is > 64 characters.', () => {
        var req = registrationReq();
        req.body.emailAddress = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@a.com';
        var res = authRes();

        userController.register(req, res);
        findSuccess({});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when the registration passwords don\'t match.', () => {
        var req = registrationReq();
        req.body.password2 = 'abc';
        var res = authRes();

        userController.register(req, res);
        findSuccess({});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when the registration password is shorter than 8 characters.', () => {
        var req = registrationReq();
        req.body.password = 'abc';
        req.body.password2 = 'abc';
        var res = authRes();

        userController.register(req, res);
        findSuccess({});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when the registration password is longer than 32 characters characters.', () => {
        var req = registrationReq();
        req.body.password = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        req.body.password2 = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        var res = authRes();

        userController.register(req, res);
        findSuccess({});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 when the registration password contains invalid characters (spaces and brackets). ', () => {
        var req = registrationReq();
        req.body.password = 'pass words';
        req.body.password2 = 'pass words';
        var res = authRes();

        userController.register(req, res);
        findSuccess({});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });
    
    it('should return status 400 when the registration doesn\'t contain all the required fields.', () => {
        var req = registrationReq();
        req.body.password = undefined;
        var res = authRes();

        userController.register(req, res);
        findSuccess({});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 500 when an internal server error is encountered while checking if the email address in a registration request is already taken', () => {
        var req = registrationReq();
        var res = authRes();

        userController.register(req, res);
        findError('registration - fetching users');

        expect(res.status).to.have.been.calledWith(500);
        expect(res.send).to.have.been.called;
    });

    it('should return status 500 when an internal server error is encountered while registering a new user', function() {
        var req = registrationReq();
        var res = authRes();

        userController.register(req, res);
        findSuccess();
        saveError('registration - saving new user');

        expect(res.status).to.have.been.calledWith(500);
        expect(res.send).to.have.been.called;
    });

    it('should return an authentication token when a user logs in with valid credentials.', () => {
        var req = loginReq();
        var res = authRes();

        userController.login(req, res);
        findSuccess({_id: '12345', password: '54321', salt: 'salt'});

        expect(res.status).to.have.been.calledWith(200);
        expect(res.json).to.have.been.calledWith({token: '1'});
    });

    it('should return status 400 if a user attempts to log in with an unknown email address', () => {
        var req = loginReq();
        var res = authRes();

        userController.login(req, res);
        findSuccess();

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 400 if a user attempts to log in with an invalid password', () => {
        var req = loginReq();
        var res = authRes();

        userController.login(req, res);
        findSuccess({_id: '12345', password: 'password', salt: 'salt'});

        expect(res.status).to.have.been.calledWith(400);
        expect(res.send).to.have.been.called;
    });

    it('should return status 500 if a user attempts to log in but an internal server error occurs', () => {
        var req = loginReq();
        var res = authRes();

        userController.login(req, res);
        findError('Login error');

        expect(res.status).to.have.been.calledWith(500);
        expect(res.send).to.have.been.called;
    });

    function setupMocks() {
        mock('jsonwebtoken', {
            sign: function () {
                return '1';
            }
        });

        mock('config', {});
        mock('password-hasher', {
            createHashAndSalt: function () {
                return {hash: 'hash', salt: 'salt'}
            },
            createHash: function () {
                return {
                    hash: {
                        toString: () => {
                            return '54321'
                        }
                    }, salt: 'salt'
                }
            }
        });

        mock('app/models/user', User);
        function User() {
            return {
                save: function () {
                    return {
                        then: function (success, error) {
                            saveSuccess = success;
                            saveError = error;
                        }
                    }
                }
            }
        }

        User.findOne = function () {
            return {
                then: function (success, error) {
                    findSuccess = success;
                    findError = error;
                }
            };
        };
    }

    function registrationReq() {
        return {
            body: {
                emailAddress: 'test@test.com',
                password: 'password',
                password2: 'password'
            }
        };
    }

    function authRes() {
        var res = {};
        res.status = sinon.stub().returns(res);
        res.send = sinon.spy();
        res.json = sinon.spy();
        return res;
    }

    function loginReq() {
        return {
            body: {
                emailAddress: 'test@test.com',
                password: 'password'
            }
        }
    }
});