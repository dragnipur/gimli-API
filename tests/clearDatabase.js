global.__base = __dirname + '/../';
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var config = require(__base + 'mongoose.config');
mongoose.connect(config.database);

var User = require(__base + 'src/app/models/user');
var Team = require(__base + 'src/app/models/team');
var TeamMember = require(__base + 'src/app/models/teamMember');
var doneCount = 0;

User.remove({}, () => {
    console.log('Removed all users.');
    doneCount++;
    done();
});

Team.remove({}, () => {
    console.log('Removed all teams.');
    doneCount++;
    done();
});

TeamMember.remove({}, () => {
    console.log('Removed all teamMembers.');
    doneCount++;
    done();
});

function done() {
    if (doneCount === 3) {
        mongoose.connection.close();
    }
}