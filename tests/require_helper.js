module.exports = function (path) {
    var base = __dirname;
    return require(base + '/../src/app/' + path);
};