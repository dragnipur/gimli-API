module.exports = {
    src: 'result/coverage/reports/**/*.json',
    options: {
        type: 'lcov',
        dir: 'result/coverage/reports',
        print: 'detail'
    }
};