module.exports = {
    unit: {
        options: {
            reporter: 'spec'
        },
        src: ['tests/unit/**/*.js']
    }
};