module.exports = {
    views: {
        expand: true,
        src: ['app/**/*.js'],
        dest: 'result/coverage/instrument/app'
    }
};