module.exports = {
    files: 'src/app/**/*.js',
    options: {
        lazy: true,
        basePath: 'result/coverage/instrument/'
    }
};