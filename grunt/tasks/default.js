module.exports = function (grunt) {
    grunt.registerTask('test', ['copy', 'env:coverage', 'instrument', 'mochaTest:unit', 'storeCoverage', 'makeReport', 'coverage', '']);
};